//
//  ModelsFactory.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/9/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import Foundation
import AVFoundation

class ModelsFactory {
    
    private static var modelsFactory : ModelsFactory?
    private static let serialQueue = DispatchQueue(label: "modelsFactoryQueue")
    private init(){}
    
    static func sharedInstance() -> ModelsFactory{
        if self.modelsFactory == nil{
            self.serialQueue.sync {
                if self.modelsFactory == nil{
                    self.modelsFactory = ModelsFactory()
                }
            }
        }
        return self.modelsFactory!
    }
    
    func getAVAudioRecorderInstance(withURL url : URL) -> AVAudioRecorder? {
        var soundRecorder : AVAudioRecorder?
        var error: NSError?
        let recordSettings = [AVSampleRateKey : NSNumber(value: Float(44100.0)),
                              AVFormatIDKey : NSNumber(value: Int32(kAudioFormatAppleLossless)),
                              AVNumberOfChannelsKey : NSNumber(value: 2),
                              AVEncoderAudioQualityKey : NSNumber(value: Int32(AVAudioQuality.max.rawValue))]
        do {
            soundRecorder =  try AVAudioRecorder(url: url, settings: recordSettings)
        } catch let error1 as NSError {
            error = error1
            soundRecorder = nil
        }
        if let err = error {
            print("AVAudioRecorder error: \(err.localizedDescription)")
        }
        
        return soundRecorder
    }
    
    func getAVAudioPalyerInstance(withURL url : URL) -> AVAudioPlayer? {
        var error: NSError?
        var soundPlayer : AVAudioPlayer?
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: url)
        } catch let error1 as NSError {
            error = error1
            soundPlayer = nil
        }
        
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        }
        return soundPlayer
    }
    
    func getRecorderViewModelInstance() -> RecorderViewModel {
        return RecorderViewModel()
    }
    
    func getDropboxCLientInstance() -> MyDropboxClient {
        return MyDropboxClient.sharedInstance()
    }
    
    func getRecordsListViewModel() -> RecordsListViewModel {
        return RecordsListViewModel()
    }
}
