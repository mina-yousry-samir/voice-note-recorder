//
//  CacheCleaner.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/9/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import Foundation

extension FileManager {
    
    func clearCache(filePath : String) {
        do {
            try self.removeItem(atPath: filePath)
        } catch {
            print(error)
        }
    }
}
