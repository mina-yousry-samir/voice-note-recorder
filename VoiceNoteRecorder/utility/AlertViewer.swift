//
//  AlertViewer.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/9/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import Foundation
import UIKit

class AlertViewer {
    
    var alert = UIAlertController()
    var timer  = Timer()
    
    func showAlertView(withMessage msg: String,onController controller: UIViewController) {
        alert = UIAlertController(title: "Voice Note Recorder", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        controller.present(alert, animated: true, completion: nil)
        //timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.dissmissAlertView), userInfo: nil, repeats: false)
    }
    
    func showAlertViewWithCustomAction(withMessage msg: String,onController controller: UIViewController, andAction action: @escaping(UIAlertAction)->()) {
        alert = UIAlertController(title: "Voice Note Recorder", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: action)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        controller.present(alert, animated: true, completion: nil)
        //timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.dissmissAlertView), userInfo: nil, repeats: false)
    }
    
    
    @objc  func dissmissAlertView() {
        alert.dismiss(animated: true, completion: nil)
    }
}
