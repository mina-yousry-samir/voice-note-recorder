//
//  DropboxClient.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/9/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import Foundation
import SwiftyDropbox

class MyDropboxClient {
    private static var myDropboxClient : MyDropboxClient?
    private static let serialQueue = DispatchQueue(label: "DropboxClientQueue")
    private init(){}
    private let dropboxClient = DropboxClientsManager.authorizedClient
    //private let dropboxClient = DropboxClient(accessToken: DROPBOX_SECRET_KEY)
    
    static func sharedInstance() -> MyDropboxClient{
        if self.myDropboxClient == nil{
            self.serialQueue.sync {
                if self.myDropboxClient == nil{
                    self.myDropboxClient = MyDropboxClient()
                }
            }
        }
        return self.myDropboxClient!
    }
    
    func uploadFileToDropbox(data : Data,fileName:String,completion : @escaping (Bool)->()) {
        let path = DROPBOX_UPLOAD_URL + fileName
        self.dropboxClient?.files.upload(path: path, input: data)
            .response { response, error in
                if let response = response {
                    completion(true)
                } else if let error = error {
                    completion(false)
                }
            }
            .progress { progressData in
                print(progressData)
        }
    }
    
    func listFilesInAppFolder(completion: @escaping (Array<Files.Metadata>?)->()) {
        let filesListResponse = self.dropboxClient?.files.listFolder(path: DROPBOX_UPLOAD_URL)
        filesListResponse?.response(completionHandler: { (response, err) in
            if let files = response?.entries{
                print(response?.entries as Any)
                completion(files)
            }
            if let _ = err {
            print(err as Any)
                completion(nil)
            }
        })
    }
    
    func downloadFile(fromPath path : String, toURL url : URL,completion: @escaping (Bool)->()){
        let destination: (URL, HTTPURLResponse) -> URL = { temporaryURL, response in
            return url
        }
        self.dropboxClient?.files.download(path: path, overwrite: true, destination: destination)
            .response { response, error in
                if let response = response {
                    print(response)
                    completion(true)
                } else if let error = error {
                    print(error)
                    completion(false)
                }
            }
            .progress { progressData in
                print(progressData)
        }
    }
}
