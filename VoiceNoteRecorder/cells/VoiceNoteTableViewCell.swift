//
//  VoiceNoteTableViewCell.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/10/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import UIKit
import SwiftyDropbox

class VoiceNoteTableViewCell: UITableViewCell {
    @IBOutlet weak var recordNameLbl: UILabel!
    
    var file : Files.Metadata?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setViews() {
        if let _ = self.file {
            self.recordNameLbl.text = self.file?.name
        }
    }
    
}
