//
//  AppDelegate.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/9/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import UIKit
import SwiftyDropbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        DropboxClientsManager.setupWithAppKey(DROPBOX_API_KEY)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let authResult = DropboxClientsManager.handleRedirectURL(url) {
            switch authResult {
            case .success:
                UserDefaults.standard.set(true, forKey: DROPBOX_AUTH_KEY)
                let notificationName = Notification.Name(rawValue: DROPBOX_AUTH_NOTIFICATION_NAME)
                NotificationCenter.default.post(name: notificationName, object: true)
            case .cancel:
                UserDefaults.standard.set(false, forKey: DROPBOX_AUTH_KEY)
                let notificationName = Notification.Name(rawValue: DROPBOX_AUTH_NOTIFICATION_NAME)
                NotificationCenter.default.post(name: notificationName, object: false)
            case .error(_, let description):
                UserDefaults.standard.set(false, forKey: DROPBOX_AUTH_KEY)
                print("Error: \(description)")
                let notificationName = Notification.Name(rawValue: DROPBOX_AUTH_NOTIFICATION_NAME)
                NotificationCenter.default.post(name: notificationName, object: false)
            }
        }
        return true
    }
    
}

