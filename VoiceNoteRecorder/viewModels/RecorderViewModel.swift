//
//  RecorderViewModel.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/9/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import Foundation
import SwiftyDropbox

class RecorderViewModel {
    
    let userDefaults = UserDefaults.standard
    let modelsFactory = ModelsFactory.sharedInstance()
    var dropboxClient : MyDropboxClient?
    var fileName : String?
    var fileUrl : URL?
    
    init() {
        self.dropboxClient = self.modelsFactory.getDropboxCLientInstance()
    }
    
    func getChacheUrl() -> URL?{
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        //print(documentsDirectory)
        let fileURL = documentsDirectory.appendingPathComponent(self.generateFileName())
        self.fileUrl = fileURL
        return fileURL
    }
    
    private func generateFileName() -> String{
        var fileName = ""
        var fileNumber = self.userDefaults.integer(forKey: FILE_COUNT_KEY)
        fileNumber = fileNumber + 1
        fileName = FILE_DEFAULT_NAME + String(fileNumber) + RECORD_EXTENSION_KEY
        self.userDefaults.set(fileNumber, forKey: FILE_COUNT_KEY)
        self.fileName = fileName
        return fileName
    }
    
    func uploadFileToDropBox(completion : @escaping (Bool)->()){
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        print(documentsDirectory)
        let fileURL = documentsDirectory.appendingPathComponent(self.fileName ?? "")
        do {
        let data = try Data(contentsOf: fileURL)
            self.dropboxClient?.uploadFileToDropbox(data: data, fileName: self.fileName ?? "", completion: {success in
                self.clearCache()
                if success {
                    completion(true)
                }else{
                    completion(false)
                }
            })
        }catch{
            print(error)
        }
    }
    
    func downloadFile(file : Files.Metadata?,completion : @escaping (Bool)->()){
        if let _ = file {
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

            let fileURL = documentsDirectory.appendingPathComponent(file?.name ?? "")
            self.dropboxClient?.downloadFile(fromPath: file?.pathDisplay ?? "", toURL: fileURL, completion: { (success) in
                if success{
                    completion(true)
                }else{
                    completion(false)
                }
            })
        }
    }
    
    func clearCache(){
        FileManager.default.clearCache(filePath: self.fileUrl?.path ?? "")
    }
}
