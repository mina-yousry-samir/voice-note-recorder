//
//  RecordsListViewModel.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/10/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import Foundation
import SwiftyDropbox

class RecordsListViewModel {
    
    let modelsFactory = ModelsFactory.sharedInstance()
    var dropboxClient : MyDropboxClient?
    var files = [Files.Metadata]()
    
    init() {
        self.dropboxClient = self.modelsFactory.getDropboxCLientInstance()
    }
    
    func getFilesNamesList(completion : @escaping (Bool)->()) {
        self.dropboxClient?.listFilesInAppFolder(completion: {files in
            if let _ = files {
                for file in files! {
                    self.files.append(file)
                }
                completion(true)
            }else{
                completion(false)
            }
        })
    }
    
    func getFilesCount() -> Int{
        return self.files.count
    }
    
    func getFile(atIndex index: Int) -> Files.Metadata {
        return self.files[index]
    }
}
