//
//  RecordViewController.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/9/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyDropbox

class RecordViewController: ParentViewController , AVAudioRecorderDelegate , AVAudioPlayerDelegate{
    
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var saveToDropBoxBtn: UIButton!
    @IBOutlet weak var startOverBtn: UIButton!
    @IBOutlet weak var gotoRecordsListBtn: UIButton!
    
    var soundRecorder : AVAudioRecorder?
    var soundPlayer : AVAudioPlayer?
    var recorderViewModel : RecorderViewModel?
    var isRecording = false
    var isPlaying = false
    var file : Files.Metadata?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.recorderViewModel = self.modelsFactory.getRecorderViewModelInstance()
        self.playBtn.isEnabled = false
        self.saveToDropBoxBtn.isEnabled = false
        self.startOverBtn.isEnabled = false
        self.recordBtn.isEnabled = false
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            self.setupRecorder()
            self.recordBtn.isEnabled = true
        case AVAudioSessionRecordPermission.denied:
            self.recordBtn.isEnabled = false
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                self.setupRecorder()
                self.recordBtn.isEnabled = true
            })
        default:
            break
        }
        
        self.registerForDropBoxAuthNotification()
        if let _ = self.file{
            self.saveToDropBoxBtn.isHidden = true
            self.startOverBtn.isHidden = true
            self.recordBtn.isHidden = true
            self.gotoRecordsListBtn.isHidden = true
            self.playBtn.isEnabled = false
            self.preparePlayer()
            self.recorderViewModel?.downloadFile(file: self.file, completion: { (success) in
                if success {
                    self.playBtn.isEnabled = true
                }else{
                    AlertViewer().showAlertView(withMessage: "error downloading voice note", onController: self)
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let _ = self.file {
            self.recorderViewModel?.clearCache()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func registerForDropBoxAuthNotification(){
        let notificationName = Notification.Name(rawValue: DROPBOX_AUTH_NOTIFICATION_NAME)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dropboxNotificationFired(notification:)), name: notificationName, object: nil)
    }
    
    private func setupRecorder() {
        if let url = self.recorderViewModel?.getChacheUrl() {
            self.soundRecorder =  self.modelsFactory.getAVAudioRecorderInstance(withURL: url)
        }
        if let _ = self.soundRecorder {
            self.soundRecorder?.delegate = self
            self.soundRecorder?.prepareToRecord()
        }
    }
    
    func preparePlayer() {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let fileURL = documentsDirectory.appendingPathComponent(file?.name ?? "")
        self.soundPlayer =  self.modelsFactory.getAVAudioPalyerInstance(withURL: fileURL)
        if let _ = self.soundPlayer {
            self.soundPlayer?.delegate = self
            self.soundPlayer?.prepareToPlay()
            self.soundPlayer?.volume = 1.0
        }
    }
    
    @IBAction func recordButtonPressed(_ sender: Any) {
        if !self.isRecording{
            self.isRecording = true
            self.recordBtn.setTitle("Stop", for: .normal)
            self.soundRecorder?.record()
        }else{
            self.soundRecorder?.stop()
        }
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        if !self.isPlaying{
            self.isPlaying = true
            self.playBtn.setTitle("Stop", for: .normal)
            self.soundPlayer?.play()
        }else{
            self.isPlaying = false
            self.playBtn.setTitle("Play", for: .normal)
            self.soundPlayer?.stop()
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        self.isRecording = false
        self.recordBtn.isEnabled = false
        self.playBtn.isEnabled = true
        self.saveToDropBoxBtn.isEnabled = true
        self.startOverBtn.isEnabled = true
        self.recordBtn.setTitle("Record", for: .normal)
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.playBtn.setTitle("Play", for: .normal)
        self.isPlaying = false
    }
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    
    @IBAction func saveToDropBoxButtonPressed(_ sender: Any) {
        self.startOverBtn.isEnabled = false
        self.recordBtn.isEnabled = false
        self.playBtn.isEnabled = false
        self.saveToDropBoxBtn.isEnabled = false
        if !UserDefaults.standard.bool(forKey: DROPBOX_AUTH_KEY){
            DropboxClientsManager.authorizeFromController(UIApplication.shared, controller: self, openURL: { (url: URL) -> Void in
                UIApplication.shared.open(url, options: [:], completionHandler: { success in
                })
            })
        }else{
            self.saveToDropbox()
        }
    }
    
    private func saveToDropbox(){
        self.recorderViewModel?.uploadFileToDropBox(completion: {success in
            if success {
                self.recordBtn.isEnabled = true
                AlertViewer().showAlertView(withMessage: "saved to dropbox successfully", onController: self)
            }else{
                self.startOverBtn.isEnabled = true
                AlertViewer().showAlertView(withMessage: "error saving to dropbox", onController: self)
            }
        })
    }
    
    @objc func dropboxNotificationFired(notification : NSNotification){
        let authorized = notification.object as? Bool
        if authorized ?? false {
            self.saveToDropbox()
        }
    }
    
    @IBAction func startOverButtonPressed(_ sender: Any) {
        self.recorderViewModel?.clearCache()
        self.startOverBtn.isEnabled = false
        self.recordBtn.isEnabled = true
        self.playBtn.isEnabled = false
        self.saveToDropBoxBtn.isEnabled = false
    }
}
