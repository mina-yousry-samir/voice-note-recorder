//
//  RecordsListViewController.swift
//  VoiceNoteRecorder
//
//  Created by Mina  on 3/10/19.
//  Copyright © 2019 Restart. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDropbox

class RecordsListViewController : ParentViewController, UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var recordsTableView: UITableView!
    
    var recordsListViewModel : RecordsListViewModel?
    var selectedFile : Files.Metadata?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.recordsListViewModel = self.modelsFactory.getRecordsListViewModel()
        self.registerCells()
        self.recordsListViewModel?.getFilesNamesList(completion: { (success) in
            if success {
                DispatchQueue.main.async {
                    self.recordsTableView.reloadData()
                }
            }else{
                AlertViewer().showAlertView(withMessage: "Error fetching files", onController: self)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func registerCells(){
        let recordCellNib = UINib.init(nibName: String(describing: VoiceNoteTableViewCell.self), bundle: nil)
        self.recordsTableView.register(recordCellNib, forCellReuseIdentifier: "recordCell")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destNavigationVc = segue.destination as! UINavigationController
        let destVc = destNavigationVc.topViewController as! RecordViewController
        destVc.file = self.selectedFile
    }
    
}

extension RecordsListViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsListViewModel?.getFilesCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.recordsTableView.dequeueReusableCell(withIdentifier: "recordCell", for: indexPath) as! VoiceNoteTableViewCell
        let file = self.recordsListViewModel?.getFile(atIndex: indexPath.row)
        cell.file = file
        cell.setViews()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedFile = self.recordsListViewModel?.getFile(atIndex: indexPath.row)
        self.performSegue(withIdentifier: PLAY_SCREEN_SEGUE, sender: self)
    }
}
